#include <iostream>
#include <stack>
#include <vector>

using namespace std;

vector<int> read_order(int len) {
  vector<int> v(len);
  cin >> v[0];
  if (v[0] == 0) {
    v.clear();
  }
  else {
    for (int i=1; i<len; ++i) cin >> v[i];
  }
  return v;
}

bool process_order(vector<int> &order) {
  stack<int> station;
  int coach=1;
  for (int i=0; i<order.size(); ++i) {
    if (!station.empty() && station.top() == order[i]) {
      station.pop();
    }
    else {
      if (coach > order[i]) return false;
      while(coach != order[i]) {
        station.push(coach);
        ++coach;
      }
      ++coach;
    }
  }
  return station.empty();
}

int main() {
  int len;
  while(cin >> len && len != 0) {
    do {
      vector<int> order = read_order(len);
      if (order.empty()) break;
      if (process_order(order)) cout << "Yes" << endl;
      else cout << "No" << endl;
    } while(true);
    cout << endl;
  }
}
