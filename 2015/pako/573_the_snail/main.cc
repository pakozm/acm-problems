#include <algorithm>
#include <iostream>

using namespace std;

/* H: height
 * U: day climb
 * D: night drop
 * F: fatigue factor in 100
 */

pair<bool,int> computeNumberOfDays(int H, int U, int D, int F) {
  int day = 0;
  float fatigue_per_day = F/100.0f * U;
  float pos = 0.0f;
  do {
    float today_climb_distance = max(0.0f, U - day*fatigue_per_day);
    ++day;
    pos += today_climb_distance;
    if (pos > H) break;
    pos = pos - D;
  } while(pos >= 0.0f);
  if (pos < 0.0f) return make_pair(false,day);
  else return make_pair(true,day);
}

int main() {
  int H, U, D, F;
  while(cin >> H >> U >> D >> F && H != 0) {
    pair<bool,int> result = computeNumberOfDays(H,U,D,F);
    if (result.first) cout << "success";
    else cout << "failure";
    cout << " on day " << result.second << endl;
  }
  return 0;
}
